const hapi = require('hapi');

const SERVER_CONFIG = require('./config/server');

const SERVER = hapi.server({
  port: SERVER_CONFIG.port,
  host: SERVER_CONFIG.host
});

const server = {
  init: async () => {
    await SERVER.start();
    console.log(`server listening on port ${SERVER_CONFIG.port}`);
  },
};
//TODO : move to new folder
const routes = {
  home:  () => {
    SERVER.route({
      method: 'GET',
      path: '/',
      handler: function(req, res)  {
        return `<h1>Hello Weekly time</h1>`
      },
    })
  }
}

//kick off
server.init();
routes.home();
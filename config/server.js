/** 
 * server configurations
*/

const SERVER_CONFIG = {
  port: 4000,
  host: 'localhost'
};

module.exports = SERVER_CONFIG